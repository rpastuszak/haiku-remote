angular.module('pl.paprikka.haiku-remote.directives.remote', []).directive('haikuRemote', [
  '$window'
  '$rootScope'

  ($window, $rootScope)->
    replace: yes
    restrict: 'AE'
    templateUrl: 'remote/remote.html'
    scope:
      remote: '='
      previewStatus: '='

    link: (scope, elm, attr) ->

      scope.remoteStatus = 'no'

      updateStatus = ->
        currentCategory = scope.previewStatus.currentCategory
        currentSlide = scope.previewStatus.currentSlide

        scope.isLastCategory  = if currentCategory is scope.previewStatus.categories.length - 1 then yes else no
        scope.isLastSlide     = if currentSlide is scope.previewStatus.categories[currentCategory]?.slides?.length - 1 then yes else no
        scope.isFirstCategory = if currentCategory is 0 then yes else no
        scope.isFirstSlide    = if currentSlide is 0 then yes else no

      $rootScope.$on 'haiku:remote:update', (e, data)->
        scope.remoteStatus = 'ready'
        console.log 'update!', data
        scope.$apply ->
          scope.previewStatus = data
          updateStatus()

      keyCodes =
        37: 'left'
        38: 'up'
        39: 'right'
        40: 'down'


      scope.goto = (direction)->
        unless scope.$$phase then scope.$apply ->
          ps = scope.previewStatus
          position =
            currentSlide: ps.currentSlide
            currentCategory: ps.currentCategory
          switch direction
            when 'left'
              position.currentCategory = Math.max (position.currentCategory - 1), 0
              position.currentSlide = 0
            when 'up'
              position.currentSlide = Math.max (position.currentSlide - 1), 0
            when 'right'
              position.currentCategory = Math.min (ps.categories.length - 1), (position.currentCategory + 1)
              position.currentSlide = 0
            when 'down'
              position.currentSlide = Math.min (ps.categories[position.currentCategory].slides.length - 1), (position.currentSlide + 1)
          scope.remote.goto position


      onKeyDown = (e) ->
        if keyCodes[e.keyCode] then scope.goto keyCodes[e.keyCode]




      $('body').on 'keydown', onKeyDown

      # TODO: create tap directive
      Hammer(elm.find('.remote__up')[0]).on 'tap',    -> scope.goto 'up'
      Hammer(elm.find('.remote__right')[0]).on 'tap', -> scope.goto 'right'
      Hammer(elm.find('.remote__down')[0]).on 'tap',  -> scope.goto 'down'
      Hammer(elm.find('.remote__left')[0]).on 'tap',  -> scope.goto 'left'

])
