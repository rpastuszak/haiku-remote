angular.module('pl.paprikka.haiku-remote.controllers.remote', [

  'common.services.Dialog'
  'common.directives.selectAll'

  ]).controller('HaikuRemoteCtrl', [

  '$scope'
  '$routeParams'
  '$rootScope'
  'Dialog'
  'Remote'

  ($scope, $routeParams, $rootScope, Dialog, Remote) ->

    $scope.previewStatus = {}

    $scope.remote = new Remote
    $scope.roomID = $routeParams.roomID
    $scope.topMessage = 'Connecting...'

    $rootScope.$on 'room:joined', (e, data)->
      console.log 'Joined: ', data
      $scope.topMessage = $routeParams.roomID
      $scope.status = 'ready'
      $scope.remote.broadcastJoinedRemote $scope.roomID

    $scope.joinRoom =  (roomID) ->
      $scope.remote.join roomID

    if $scope.roomID then $scope.joinRoom $scope.roomID

])
