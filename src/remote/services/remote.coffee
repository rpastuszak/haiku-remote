# ## Remote
#
# ### How to initialise connection with player:
#
# -> outbound
# <- inbound
#
# 1. `room:join` ->
# 2. `room:joined` <-
# 3. `room:remoteJoined` ->
# 4. `remote:update` <-
#
# ### Control events:
#
# `remote` ->


angular.module('pl.paprikka.haiku-remote.services.remote', []).factory('Remote', [

  'WebSockets'
  '$rootScope'

  ( WebSockets, $rootScope ) ->
    SOCKET_LOCATION = haiku.config.hubURL
    socket = WebSockets.connect SOCKET_LOCATION
    class Remote
      room: null
      constructor: ->
        socket.on 'room:joined', (data) =>
          console.log 'Remote::room joined'
          @room = data.room
          $rootScope.$emit 'room:joined', data

        socket.on 'remote:update', (data)->
          console.log 'Remote::update received', data
          $rootScope.$emit 'haiku:remote:update', data.data
        # socket.emit 'remote:update', {data, room}

      goto: (position)->
        console.log position
        message =
          command:  'position'
          params:   position
          room:     @room
        console.log 'Remote::control goto'
        socket.emit 'remote', message

      go:( direction ) ->
        console.log {direction}
        message =
          command: 'direction'
          params:
            direction: direction
          room: @room
        console.log 'Remote::control direction (go)'
        socket.emit 'remote', message

      join:     (room) ->
        console.log 'Remote::joining room'
        socket.emit 'room:join', { room: room, isRemote: yes }

      broadcastJoinedRemote: (room) ->
        console.log 'Remote::broadcast join'
        socket.emit 'remote:remoteJoined', { room }

      leave:    (room, cb) ->
        console.log 'Remote::leave'
        socket.emit 'room:leave', { room: room }

      request:  (room) ->
        socket.emit 'room:request', { room: room }

      # create:   (room) -> socket.emit 'room:create', { room: room }

    Remote
])
