angular.module('common.services.Dialog', []).service('Dialog', [

  '$window'

  ($window)->
    Dialog = 
      alert: (title) ->
        $window.alert title
      
      
      prompt: (title, callback)->
        result = $window.prompt title
        callback? result

])