angular.module('common.directives.selectAll', []).directive('haikuSelectAll', ->
  (scope, elm)->
    elm.on 'focus', -> @select()
)