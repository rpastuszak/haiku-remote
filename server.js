var connect = require('connect');
var serveStatic = require('serve-static');
var PORT = process.env.PORT || 8080;
console.log('Starting Haiku Remote @ ' + PORT);
connect().use(serveStatic(__dirname + '/public')).listen(PORT);
