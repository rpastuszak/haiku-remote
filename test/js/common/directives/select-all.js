angular.module('common.directives.selectAll', []).directive('haikuSelectAll', function() {
  return function(scope, elm) {
    return elm.on('focus', function() {
      return this.select();
    });
  };
});
