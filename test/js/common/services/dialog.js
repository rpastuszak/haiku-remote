angular.module('common.services.Dialog', []).service('Dialog', [
  '$window', function($window) {
    var Dialog;
    return Dialog = {
      alert: function(title) {
        return $window.alert(title);
      },
      prompt: function(title, callback) {
        var result;
        result = $window.prompt(title);
        return typeof callback === "function" ? callback(result) : void 0;
      }
    };
  }
]);
