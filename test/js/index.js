'use strict';
var App;

App = angular.module('app', ['templates', 'ngCookies', 'ngResource', 'app.controllers', 'app.common.webSockets', 'pl.paprikka.haiku-remote']);

App.config([
  '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.when('/404', {
      templateUrl: 'pages/404.html'
    }).when('/:roomID', {
      templateUrl: 'pages/remote.html',
      controller: 'HaikuRemoteCtrl'
    }).otherwise({
      redirectTo: '/404'
    });
    return $locationProvider.html5Mode(false);
  }
]).run();
