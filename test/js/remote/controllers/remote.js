angular.module('pl.paprikka.haiku-remote.controllers.remote', ['common.services.Dialog', 'common.directives.selectAll']).controller('HaikuRemoteCtrl', [
  '$scope', '$routeParams', '$rootScope', 'Dialog', 'Remote', function($scope, $routeParams, $rootScope, Dialog, Remote) {
    $scope.previewStatus = {};
    $scope.remote = new Remote;
    $scope.roomID = $routeParams.roomID;
    $scope.topMessage = 'Connecting...';
    $rootScope.$on('room:joined', function(e, data) {
      console.log('Joined: ', data);
      $scope.topMessage = $routeParams.roomID;
      return $scope.remote.broadcastJoinedRemote($scope.roomID);
    });
    $scope.joinRoom = function(roomID) {
      return $scope.remote.join(roomID);
    };
    if ($scope.roomID) {
      return $scope.joinRoom($scope.roomID);
    }
  }
]);
