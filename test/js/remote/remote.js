angular.module('pl.paprikka.haiku-remote.directives.remote', []).directive('haikuRemote', [
  '$window', '$rootScope', function($window, $rootScope) {
    return {
      replace: true,
      restrict: 'AE',
      templateUrl: 'remote/remote.html',
      scope: {
        remote: '=',
        previewStatus: '='
      },
      link: function(scope, elm, attr) {
        var keyCodes, onKeyDown, updateStatus;
        scope.remoteStatus = 'no';
        updateStatus = function() {
          var currentCategory, currentSlide, _ref, _ref1;
          currentCategory = scope.previewStatus.currentCategory;
          currentSlide = scope.previewStatus.currentSlide;
          scope.isLastCategory = currentCategory === scope.previewStatus.categories.length - 1 ? true : false;
          scope.isLastSlide = currentSlide === ((_ref = scope.previewStatus.categories[currentCategory]) != null ? (_ref1 = _ref.slides) != null ? _ref1.length : void 0 : void 0) - 1 ? true : false;
          scope.isFirstCategory = currentCategory === 0 ? true : false;
          return scope.isFirstSlide = currentSlide === 0 ? true : false;
        };
        $rootScope.$on('haiku:remote:update', function(e, data) {
          scope.remoteStatus = 'ready';
          console.log('update!', data);
          return scope.$apply(function() {
            scope.previewStatus = data;
            return updateStatus();
          });
        });
        keyCodes = {
          37: 'left',
          38: 'up',
          39: 'right',
          40: 'down'
        };
        scope.goto = function(direction) {
          if (!scope.$$phase) {
            return scope.$apply(function() {
              var position, ps;
              ps = scope.previewStatus;
              position = {
                currentSlide: ps.currentSlide,
                currentCategory: ps.currentCategory
              };
              switch (direction) {
                case 'left':
                  position.currentCategory = Math.max(position.currentCategory - 1, 0);
                  break;
                case 'up':
                  position.currentSlide = Math.max(position.currentSlide - 1, 0);
                  break;
                case 'right':
                  position.currentCategory = Math.min(ps.categories.length - 1, position.currentCategory + 1);
                  break;
                case 'down':
                  position.currentSlide = Math.min(ps.categories[position.currentCategory].slides.length - 1, position.currentSlide + 1);
              }
              return scope.remote.goto(position);
            });
          }
        };
        onKeyDown = function(e) {
          if (keyCodes[e.keyCode]) {
            return scope.goto(keyCodes[e.keyCode]);
          }
        };
        $('body').on('keydown', onKeyDown);
        Hammer(elm.find('.remote__up')[0]).on('tap', function() {
          return scope.goto('up');
        });
        Hammer(elm.find('.remote__right')[0]).on('tap', function() {
          return scope.goto('right');
        });
        Hammer(elm.find('.remote__down')[0]).on('tap', function() {
          return scope.goto('down');
        });
        return Hammer(elm.find('.remote__left')[0]).on('tap', function() {
          return scope.goto('left');
        });
      }
    };
  }
]);
