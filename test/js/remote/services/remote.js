angular.module('pl.paprikka.haiku-remote.services.remote', []).factory('Remote', [
  'WebSockets', '$rootScope', function(WebSockets, $rootScope) {
    var HUB_LOCATION, Remote, SOCKET_LOCATION, socket;
    HUB_LOCATION = 'http://haiku-hub.herokuapp.com:80';
    SOCKET_LOCATION = location.hostname.split('.')[0] === '192' ? location.hostname + ':8082' : HUB_LOCATION;
    socket = WebSockets.connect(SOCKET_LOCATION);
    Remote = (function() {
      Remote.prototype.room = null;

      function Remote() {
        var _this = this;
        socket.on('room:joined', function(data) {
          console.log('Remote::room joined');
          _this.room = data.room;
          return $rootScope.$emit('room:joined', data);
        });
        socket.on('remote:update', function(data) {
          console.log('Remote::update received', data);
          return $rootScope.$emit('haiku:remote:update', data.data);
        });
      }

      Remote.prototype.goto = function(position) {
        var message;
        console.log(position);
        message = {
          command: 'position',
          params: position,
          room: this.room
        };
        console.log('Remote::control goto');
        return socket.emit('remote', message);
      };

      Remote.prototype.go = function(direction) {
        var message;
        console.log({
          direction: direction
        });
        message = {
          command: 'direction',
          params: {
            direction: direction
          },
          room: this.room
        };
        console.log('Remote::control direction (go)');
        return socket.emit('remote', message);
      };

      Remote.prototype.join = function(room) {
        console.log('Remote::joining room');
        return socket.emit('room:join', {
          room: room,
          isRemote: true
        });
      };

      Remote.prototype.broadcastJoinedRemote = function(room) {
        console.log('Remote::broadcast join');
        return socket.emit('remote:remoteJoined', {
          room: room
        });
      };

      Remote.prototype.leave = function(room, cb) {
        console.log('Remote::leave');
        return socket.emit('room:leave', {
          room: room
        });
      };

      Remote.prototype.request = function(room) {
        return socket.emit('room:request', {
          room: room
        });
      };

      return Remote;

    })();
    return Remote;
  }
]);
